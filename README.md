<!-- vim: set ff=unix tw=80 ts=4 sw=4 ai expandtab: -->

This project contains a GitLab CI script at `.gitlab-ci.yml` with jobs `build`
and `deploy` that respectively build binary packages from RPM projects and copy
the packages to targets via SCP.  The script can be called remotely by a GitLab
server with access to a GitLab runnner able to build the RPM package, or locally
by a work machine equipped with the `gitab-runner` executable and able to build
the RPM package.  Thus, builds and deployments can be tested locally before
pushing to a GitLab server.

Package Parameters
==================

Ordinarily, the package parameters of an RPM project, such as the name, version
and release of the package it produces, are specified in the project's `.spec`
file.  This, however, has two shortcomings.  Firstly, it hampers writing a
single, parameterised `.spec` file for a family of very similar RPM projects,
such as a suite of Nagios plug-ins.  Secondly, and more seriously, maintaining
the release value of a package requires editing the `.spec` file each time a
modified package is produced.  This is an onerous, error-prone and often
overlooked task that can lead to different packages having identical parameters
if carried out incorrectly.

The job `build` of the GitLab CI script provides a mechanism to overcome these
shortcomings.  The job extracts the parameters `«name»`, `«version»`,
`«release»` and `«commit_id»` from an RPM project's Git repository, where

-   `«name»-«version»` is the most recent annotated Git tag,

-   `«release»` is the number of Git commits (possibly 0) since
    `«name»-«version»`, and

-   `«commit_id»` is the first seven characters of the commit ID of the `HEAD`
    Git commit,

In fact, the job will fail if the repository has no annotated tag, or the most
recent annotated tag does not contain a `-` character.

The job makes these parameters available to the RPM build process as the RPM
variables `%{NAME}`, `%{VERSION}`, `%{RELEASE}` and `%{COMMIT_ID}`.  Thus, if
the `.spec` file of an RPM project contains the directives

    Name: %{NAME}
    Version: %{VERSION}
    Release: %{RELEASE}

and the project's Git repository is appropriately tagged, then the full name of
the package created by the project will be

    «name»-«version»-«release»

Notice, in particular, that the release value of the package is automatically
incremented each time the project is modified and the modifications committed to
its repository.

The intention here is that `«name»-«version»` be set to the name and version of
the software being packaged.  For example, suppose that the `.spec` file of an
RPM project contains the directives

    Name: %{NAME}
    Version: %{VERSION}
    Release: %{RELEASE}

Suppose also that version `1.2.3` of the software `hi` is first committed to the
Git repository of the project at the commit with the ID `cdbd55a`, and that this
commit is given the annotated tag `hi-1.2.3` by, for example, calling

    git tag -a 'hi-1.2.3' -m 'Add version 1.2.3 of hi' cdbd55a

(The option `-m` is obligatory and gives the tag a message, but the content of
the message is for for the benefit of project members, and plays no role here.)
The job `build` gives the package built from that commit the full name

    hi-1.2.3-0
    
If the next commit has the ID `77957b8` then the job gives the package built
from that commit the full name

    hi-1.2.3-1
    
Now suppose that version `2.3.4` of the software is committed to the project's
repository at the next commit, with the ID `acb3c75`, and that the commit is
given the annotated tag `hi-2.3.4` by, for example, calling

    git tag -a 'hi-2.3.4' -m 'Update to version 2.3.4 of hi' acb3c75

The job now resets `«release»` to `0` and gives the package built from that
commit the full name

    hi-2.3.4-0

If the ID of a commit is included in a package's name, then the name of the
package indicates which commit produced the package.  For example, if the
`.spec` file of the example RPM project had contained the directives

    Name: %{NAME}
    Version: %{VERSION}
    Release: %{RELEASE}_%{COMMIT_ID}

then the packages it produced would have had the names

-   `hi-1.2.3-0_cdbd55a`,

-   `hi-1.2.3-1_77957b8`, and

-   `hi-2.3.4-0_acb3c75`

respectively.  The commit ID can be invaluable, for example, if trying to
establish the provenance of an "orphaned" package.

Usage
=====

Local and Remote
----------------

Copy the GitLab CI script to a file at `.gitlab-ci.yml` at the root of an RPM
project, alongside the project's `SOURCES` and `SPECS` directories.  The project
should thus have a structure similar to

    .
    ├── .gitlab-ci.yml
    ├── SOURCES
    │   └── ‥
    └── SPECS
        └── ‥

Add and commit the script and project to a Git repository at the project's root,
and ensure that the repository has a most recent annotated tag of the form
`«name»-«version»` by, for example, calling

    git tag -a 'hi-1.2.3' -m 'hi-1.2.3' 

to give the current `HEAD` commit the annotated tag `hi-1.2.3`.

In order to use the package parameters that the job `build` of the script makes
available to the RPM build process, ensure that the project's `.spec` file
contains appropriate directives, such as

    Name: %{NAME}
    Version: %{VERSION}
    Release: %{RELEASE}

or

    Name: %{NAME}
    Version: %{VERSION}
    Release: %{RELEASE}_%{COMMIT_ID}

Local Only
----------

Ensure that `gitlab-runner` is installed, and change the working directory to
the project's root.  Call

    gitlab-runner exec shell build

in order to try to build a package according to a file at `SPECS/rpm.spec`,
otherwise call

    SPEC="«file»" gitlab-runner exec shell build

in order to try to build a package according to the file at `SPECS/«file».spec`.
If successful, the call copies the package to the directory at
`builds/0/project-0/deploy`.

Then, optionally, call

    TARGET_«suffix_1»=«target_1» ... TARGET_«suffix_n»=«target_n» gitlab-runner exec shell deploy

in order to copy the package in the directory at `builds/0/project-0/deploy` to
the targets `«target_1»`, ..., `«target_n»` via SCP.  Note that

-   each of `TARGET_«suffix_1»`, ..., `TARGET_«suffix_n»` must be a valid
    Bourne shell variable name,
     
-   each of `«suffix_1»`, ..., `«suffix_n»` should be distinct, and

-   each of `«target_1»`, ..., `«target_n»` must be writable.

**Note**

Each successful call of `gitlab-runner exec shell «job»` creates new directories
at `builds` and `cache` at the root of the project, and gives the project a
structure similar to

    .
    ├── .gitlab-ci.yml
    ├── builds
    │   └── 0
    │       ├── project-0
    │       │   └── ‥
    │       └── project-0.tmp
    │           └── ‥
    ├── cache
    │   └── ‥
    ├── SOURCES
    │   └── ‥
    └── SPECS
        └── ‥

(Git can ignore the new directories.)  The call then

-   clones the Git repository at the project's root to the directory at
    `builds/0/project-0`, and

-   executes in the directory at `builds/0/project-0` the job `«job»` in the
    script at `.gitlab-ci.yml` in the project's root

using the directory at `builds/0/project-0.tmp` for temporary files, and caching
objects in the directory at `cache`.  Notice that `«job»` is read from the
**root** directory, but executes in the **clone** directory.  Thus, `«job»`
comes from the root directory but can only act upon resources in the clone
directory, resources that can only come from the **`HEAD`** **commit** of the
project's repository — `«job»` cannot access any unstaged or uncommited
resources in the root directory.  This frequently leads to many local commits
before reaching a succesful build, and it may be politic to tidy the history of
the local repository with `git rebase -i` before pushing it!

Remote Only
-----------

Within the project's GitLab page:

-   Under `Settings » CI/CD » Runners`, register a GitLab runner that can build
    a package.

-   If the project's `.spec` file is **not** at `SPECS/rpm.spec`, then under
    `Settings » CI/CD » Environment variables`, create the environment variable
    `SPEC` and set its value to `«file»` in order to build a package according
    to the file at `SPECS/«file».spec`.

-   For each target `«target_i»` in `«target_1»`, ..., `«target_n»`:

    -   under `Settings » CI/CD » Environment variables`, create an environment
        variable `TARGET_«suffix_i»` and set its value to `«target_i»` in order to
        copy the package to the target `«target_i»` via SCP.

    Note that

    -   each of `TARGET_«suffix_1»`, ..., `TARGET_«suffix_n»` must be a valid
        Bourne shell variable name,
         
    -   each of `«suffix_1»`, ..., `«suffix_n»` should be distinct, and
    
    -   each of `«target_1»`, ..., `«target_n»` must be writable by the GitLab
        daemon user (usually `git`).
    
The GitLab CI script only builds and deploys commits to the `master` Git branch.
Modify the `only:` directives in the jobs `build` and|or `deploy` of the script
in order to build and|or deploy other commits.

Appendix: Dependencies and Cache
--------------------------------

I purposely wrote the GitLab CI script so that it can be called remotely by a
GitLab server with access to a GitLab runnner able to build an RPM package, or
locally by a work machine equipped with the `gitab-runner` executable and able
to build an RPM package.  Unfortunately, the local call

    gitlab-runner exec shell «job»

only runs the job `«job»` of the script, and carries no dependencies from that
job forward to subsequent calls of `gitlab-runner exec shell`.  For example, if

    gitlab-runner exec shell «job_1»

produces a dependency `«object»` that

    gitlab-runner exec shell «job_2»

consumes, then the call

    gitlab-runner exec shell «job_1»
    gitlab-runner exec shell «job_2»

fails, because `«object»` does not persist after `gitlab-runner exec shell
«job_1»` exits, and hence is not available to `gitlab-runner exec shell
«job_2»`.  To avoid this shortcoming, the script uses an on-disk cache and no
dependencies.

